import React from "react"
import { NavigationContainer } from "@react-navigation/native"
import { useFonts } from "expo-font"
import { Provider } from "react-redux"

// Local imports
import { MainNativeStack } from "./navigation/MainNativeStack/MainNativeStack"
import { GestureHandlerRootView } from "react-native-gesture-handler"
import { store } from "./redux/store"

export default function App() {
  const [fontsLoaded] = useFonts({
    Urbanist: require("./assets/fonts/Urbanist-VariableFont_wght.ttf"),
  })

  if (fontsLoaded)
    return (
      <Provider store={ store }>
        <GestureHandlerRootView style={ { flex: 1 } }>
          <NavigationContainer>
            <MainNativeStack />
          </NavigationContainer>
        </GestureHandlerRootView>
      </Provider>
    )
  else return <></>
}
