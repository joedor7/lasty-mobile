import { createSlice, PayloadAction } from "@reduxjs/toolkit"

// This is the interface for the global user state
interface UserState {
  username: string | undefined
  email: string | undefined
  modifiedField?: keyof UserState
  otp?: string
}

const initialState: UserState = {
  username: undefined,
  email: undefined
}

interface ISetUserCredentials {
  modifiedField: keyof UserState
  email?: string
  username?: string
  otp?: string
}

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    getUser(state) { return state },
    setUserCredentials(state, action: PayloadAction<ISetUserCredentials>) {
      switch (action.payload.modifiedField) {
        case "email":
          return { ...state, email: action.payload.email }
        case "otp":
          return { ...state, otp: action.payload.otp }
        default:
          break
      }
    }
  }
})

export const { getUser, setUserCredentials } = userSlice.actions
export default userSlice.reducer