import { configureStore } from "@reduxjs/toolkit"

// Local imports
import userReducer from "./features/user/userSlice"
import { apiSlice } from "./apiSlices/apiSlice"

export const store = configureStore({
  reducer: {
    user: userReducer,
    [apiSlice.reducerPath]: apiSlice.reducer,
  },
  middleware(getDefaultMiddleware) {
    return getDefaultMiddleware().concat(apiSlice.middleware)
  },
})

export type AppDispatch = typeof store.dispatch
export type RootState = ReturnType<typeof store.getState>