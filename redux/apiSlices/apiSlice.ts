import { createApi, FetchArgs, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

import { MAIN_API_URL } from "@env"

const headers = {
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Methods': '*',
  'Content-Type': 'application/json',
}

const baseQueryOptions = fetchBaseQuery({
  baseUrl: MAIN_API_URL,
  headers,
  // prepareHeaders: (headers) => {
  //   const token = <storage>.getItem('') || null
  //   if (token) {
  //     headers.set('authorization', `Bearer ${token}`)
  //   }
  //   return headers
  // },
})

const baseQueryWithAuth = async (args: FetchArgs, api: any, extraOptions: any) => {
  try {

    let result: any = await baseQueryOptions(args, api, extraOptions)

    if (result?.error?.status >= 400 && result?.error?.status !== 401)
      throw "Error fetching the data"

    if (result?.error?.status === 401)
      console.log(result)

    return result

  } catch (error) {
    console.log(error)
  }
  //   const refreshToken = <storage>.getItem('')
  //   if (refreshToken) {
  //     try {
  //       const response: any = await fetch(`${process.env.REACT_APP_API_BASE_URL}api/auth/refresh`, {
  //         method: 'POST',
  //         headers,
  //         body: JSON.stringify({
  //           refreshToken,
  //         }),
  //       })
  //       const accessToken = await response.json()

  //       if (accessToken) {
  //         <storage>.setItem('supelle_accessToken', accessToken.accessToken)
  //       } else {
  //         throw('Session timed out,sign in again')
  //       }
  //       result = await baseQuery(args, api, extraOptions)
  //     } catch (error: any) {
  //       throw('Something went wrong,sign in again')
  //     }
  //   }

}

export const apiSlice = createApi({
  baseQuery: baseQueryWithAuth,
  endpoints: builder => ({
  }),
})