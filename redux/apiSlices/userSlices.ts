import { ICreateUserForm } from '../../types/User/mainUserTypes'
import { apiSlice } from './apiSlice'

export const authApiSlice = apiSlice.injectEndpoints({
  endpoints: builder => ({
    getOTP: builder.mutation<{ msg: string }, { email: string }>({
      query: body => ({
        url: '/user/generate_otp',
        method: 'POST',
        body,
      }),
    }),
    verifyOTP: builder.mutation<{ msg: string, user: any }, { email: string, otp: string }>({
      query: credentials => ({
        url: 'user/verify_otp',
        method: 'POST',
        body: {
          ...credentials,
        },
      }),
    }),
    createUser: builder.mutation<{ msg: string, data?: any }, ICreateUserForm>({
      query: credentials => ({
        url: 'user/register',
        method: 'POST',
        body: {
          ...credentials,
        },
      }),
    }),
  }),
})

export const { useVerifyOTPMutation, useGetOTPMutation, useCreateUserMutation } = authApiSlice