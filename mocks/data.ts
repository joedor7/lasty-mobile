import { colors } from "../theme/colors"

interface IUser {
  username: string
  email: string
  pfp: any
}

export const userMock: IUser = {
  username: "Sheekate",
  email: "sheekate@gmail.com",
  pfp: require("../assets/images/pfp_mock.png"),
}

export const quickNoteMock = [
  {
    note_idea:
      "Impact of artificial intelligence on the future of work and its potential benefits and challenges?",
    people_who_wrote: [
      require("../assets/images/pfp1.png"),
      require("../assets/images/pfp2.png"),
      require("../assets/images/pfp3.png"),
    ],
  },
  {
    note_idea:
      "Discuss your latest travel adventure or a fun hobby you've been enjoying lately",
    people_who_wrote: [
      require("../assets/images/pfp1.png"),
      require("../assets/images/pfp2.png"),
      require("../assets/images/pfp3.png"),
    ],
  },
]

export const pinnedNotesMock = [
  // {
  //   title: "10 things not to do at work",
  //   content: "1. You must never disrespect a client",
  //   writer: [require("../assets/images/pfp_mock.png")],
  //   last_modified: "September 10, 2023",
  //   color: colors.PRIMARY,
  //   tag: "Work",
  // },
  // {
  //   title: "Things learned from Marcus Aurelius",
  //   content:
  //     "Beware of what people think of you, but do not let them influence your thinking",
  //   writer: [require("../assets/images/pfp_mock.png")],
  //   last_modified: "September 10, 2023",
  //   color: colors.SECONDARY_ORANGE,
  //   tag: "Personal",
  // },
]

export const recentNotesMock = [
  // {
  //   title: "10 things not to do at work",
  //   content: `You must never disrespect a client Engage in gossip or spread rumors about people Display negative attitude and bring down the team`,
  //   writer: [require("../assets/images/pfp_mock.png")],
  //   last_modified: "September 10, 2023",
  //   color: colors.PRIMARY,
  //   tag: "Work",
  // },

  // {
  //   title: "Reflecting on Life: Marcus Aurelius' Meditations 📖✨",
  //   content: `Marcus Aurelius' Meditations is a timeless collection of philosophical reflections by the Roman emperor. It offers profound insights on self-reflection, inner peace, and the pursuit of virtue. Dive into the pages of this remarkable work and discover the wisdom that has inspired countless individuals throughout history.`,
  //   writer: [require("../assets/images/pfp_mock.png")],
  //   last_modified: "September 10, 2023",
  //   color: colors.SECONDARY_ORANGE,
  //   tag: "Personal",
  // },
  // {
  //   title: "Reflecting on Life: Marcus Aurelius' Meditations 📖✨",
  //   content: `Marcus Aurelius' Meditations is a timeless collection of philosophical reflections by the Roman emperor. It offers profound insights on self-reflection, inner peace, and the pursuit of virtue. Dive into the pages of this remarkable work and discover the wisdom that has inspired countless individuals throughout history.`,
  //   writer: [require("../assets/images/pfp_mock.png")],
  //   last_modified: "September 10, 2023",
  //   color: colors.SECONDARY_ORANGE,
  //   tag: "Personal",
  // },
  // {
  //   title: "Reflecting on Life: Marcus Aurelius' Meditations 📖✨",
  //   content: `Marcus Aurelius' Meditations is a timeless collection of philosophical reflections by the Roman emperor. It offers profound insights on self-reflection, inner peace, and the pursuit of virtue. Dive into the pages of this remarkable work and discover the wisdom that has inspired countless individuals throughout history.`,
  //   writer: [require("../assets/images/pfp_mock.png")],
  //   last_modified: "September 10, 2023",
  //   color: colors.SECONDARY_ORANGE,
  //   tag: "Personal",
  // },
  // {
  //   title: "Reflecting on Life: Marcus Aurelius' Meditations 📖✨",
  //   content: `Marcus Aurelius' Meditations is a timeless collection of philosophical reflections by the Roman emperor. It offers profound insights on self-reflection, inner peace, and the pursuit of virtue. Dive into the pages of this remarkable work and discover the wisdom that has inspired countless individuals throughout history.`,
  //   writer: [require("../assets/images/pfp_mock.png")],
  //   last_modified: "September 10, 2023",
  //   color: colors.SECONDARY_ORANGE,
  //   tag: "Personal",
  // },
  // {
  //   title: "Reflecting on Life: Marcus Aurelius' Meditations 📖✨",
  //   content: `Marcus Aurelius' Meditations is a timeless collection of philosophical reflections by the Roman emperor. It offers profound insights on self-reflection, inner peace, and the pursuit of virtue. Dive into the pages of this remarkable work and discover the wisdom that has inspired countless individuals throughout history.`,
  //   writer: [require("../assets/images/pfp_mock.png")],
  //   last_modified: "September 10, 2023",
  //   color: colors.SECONDARY_ORANGE,
  //   tag: "Personal",
  // },
]
