export const OTP_FLOW_TEXT = {
  ENTER_OTP_PAGE_TITLE: "OTP verification",
  ENTER_OTP_PAGE_SUB:
    "A One Time Password (OTP) has been sent to your email address ",
}
