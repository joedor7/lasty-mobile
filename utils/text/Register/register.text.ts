export const registerScreenText = {
  TITLE: "Create an account",
  GET_STARTED_TEXT: "Welcome to Lasty",
  SECTION_DES:
    "We're so happy you are here, to continue please enter your email address",
};
