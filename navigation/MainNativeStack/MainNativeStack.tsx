import React from "react"
import { createNativeStackNavigator } from "@react-navigation/native-stack"

// local imports
import EnterEmailScreen from "../../screens/OTP_flow/EnterEmailScreen/EnterEmailScreen"
import OTPVerificationScreen from "../../screens/OTP_flow/OTPVerificationScreen/OTPVerificationScreen"
import { MainNativeStackParamList } from "../../types/NavigationTypes/MainNativeStackTypes"
import RegisterScreen from "../../screens/RegisterScreen"
import Home from "../../screens/Home/Home"
import NoteViewer from "../../screens/NoteViewer/NoteViewer"

export function MainNativeStack() {
  const Stack = createNativeStackNavigator<MainNativeStackParamList>()

  return (
    <Stack.Navigator>
      <Stack.Screen
        name="OTP Flow"
        component={ EnterEmailScreen }
        options={ {
          headerShown: false,
        } }
      />
      <Stack.Screen
        name="OTP Verification"
        component={ OTPVerificationScreen }
        options={ {
          headerShown: false,
        } }
      />

      <Stack.Screen
        name="Register"
        component={ RegisterScreen }
        options={ {
          headerShown: false,
          gestureEnabled: false,
        } }
      />

      <Stack.Screen
        name="Home"
        component={ Home }
        options={ {
          headerShown: false,
          gestureEnabled: false,
        } }
      />

      <Stack.Screen
        name="Create Note"
        component={ NoteViewer }
        options={ {
          headerShown: false,
        } }
      />
    </Stack.Navigator>
  )
}
