import React from "react";
import { Image, StyleSheet, View } from "react-native";

function IllustrationSection() {
  return (
    <View style={styles.container}>
      <Image
        source={require("../../../assets/images/otp_sent.png")}
        style={styles.img}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
  },
  img: {
    width: 90,
    height: 90,
  },
});

export default IllustrationSection;
