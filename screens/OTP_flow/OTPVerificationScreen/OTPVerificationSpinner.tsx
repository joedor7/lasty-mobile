import React from "react"
import LottieView from "lottie-react-native"
import { StyleSheet, View } from "react-native"

export default function OTPVerificationSpinner() {
  return (
    <View>
      <LottieView
        source={ require("../../../assets/lotties/loader_animation.json") }
        style={ styles.animation }
        autoPlay
        hardwareAccelerationAndroid
        onAnimationFailure={ () => { console.log("Animation failed") } }
      />
    </View>
  )
}

const styles = StyleSheet.create({
  animation: {
    width: 100,
    height: 100,
  },
})