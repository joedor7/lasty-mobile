import { useRef, useState } from "react"
import { TextInput } from "react-native"

export type OnChangeHandlerParaTypes = {
  inputIndex: 1 | 2 | 3 | 4
  text: string
}

export type TOtpIndex = {
  1: string,
  2: string,
  3: string,
  4: string
}

function useFormSection() {

  const [otpIndexes, setOtpIndex] = useState<TOtpIndex>({
    1: "",
    2: "",
    3: "",
    4: ""
  })

  const firstInputRef = useRef<TextInput>(null)
  const secondInputRef = useRef<TextInput>(null)
  const thirdInputRef = useRef<TextInput>(null)
  const fourthInputRef = useRef<TextInput>(null)

  function onChangeTextHandler(params: OnChangeHandlerParaTypes) {
    switch (params.inputIndex) {
      case 1:
        params.text && secondInputRef?.current?.focus()
        setOtpIndex((prev) => ({ ...otpIndexes, [1]: params.text }))
        break
      case 2:
        params.text
          ? thirdInputRef?.current?.focus()
          : firstInputRef?.current?.focus()
        setOtpIndex((prev) => ({ ...otpIndexes, [2]: params.text }))
        break
      case 3:
        params.text
          ? fourthInputRef?.current?.focus()
          : secondInputRef?.current?.focus()
        setOtpIndex((prev) => ({ ...otpIndexes, [3]: params.text }))
        break
      case 4:
        !params.text && thirdInputRef?.current?.focus()
        setOtpIndex((prev) => ({ ...otpIndexes, [4]: params.text }))
        break
      default:
        break
    }
  }

  return {
    firstInputRef,
    secondInputRef,
    thirdInputRef,
    fourthInputRef,
    onChangeTextHandler,
    otpIndexes,
  }
}

export default useFormSection
