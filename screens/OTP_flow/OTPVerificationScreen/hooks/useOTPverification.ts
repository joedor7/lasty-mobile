import { useNavigation } from '@react-navigation/native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'

// Local imports
import { useVerifyOTPMutation } from '../../../../redux/apiSlices/userSlices'
import { MainNativeStackParamList } from '../../../../types/NavigationTypes/MainNativeStackTypes'
import { TOtpIndex } from './userFormSection'
import { useAppDispatch } from '../../../../redux/hooks'
import { setUserCredentials } from '../../../../redux/features/user/userSlice'
import { useState } from 'react'

interface IHandleVerifyOtp {
  email: string,
  otp: string
}

export default function useOTPVerification() {

  const [verifyOtp, { isLoading }] = useVerifyOTPMutation()
  const dispatch = useAppDispatch()

  const [shouldOTPFormBeRed, setShouldOTPFormBeRed] = useState(false)

  const navigation =
    useNavigation<NativeStackNavigationProp<MainNativeStackParamList>>()

  function transformOTPToString(rawOTP: TOtpIndex) {
    return `${rawOTP[1]}${rawOTP[2]}${rawOTP[3]}${rawOTP[4]}`
  }

  async function handleVerifyOTP({ email, otp }: IHandleVerifyOtp) {
    if (isLoading) return

    try {
      const res = await verifyOtp({ email, otp }).unwrap()

      if (!isLoading && res.msg === "Success") {
        dispatch(setUserCredentials({ modifiedField: "otp", otp }))
        navigation.navigate("Home")
      }

      else if (!isLoading && res.msg === "OTP not valid") setShouldOTPFormBeRed(true)

      else if (!isLoading && res.msg === "Create an account") {
        dispatch(setUserCredentials({ modifiedField: "otp", otp }))
        navigation.navigate("Register")
      }

    }
    catch (error) {
      console.log(error)
    }
  }

  return {
    handleVerifyOTP,
    isLoading,
    transformOTPToString,
    shouldOTPFormBeRed,
    setShouldOTPFormBeRed
  }
}