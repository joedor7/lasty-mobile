import React from "react"
import { StyleSheet, TextInput, View } from "react-native"

// Local imports
import { OnChangeHandlerParaTypes, TOtpIndex } from "./hooks/userFormSection"
import { NUMBER_OF_OTP_DIGITS } from "../../../utils/constants/OTPRelatedConstants"
import { colors } from "../../../theme/colors"

interface Props {
  firstInputRef: React.RefObject<TextInput>
  secondInputRef: React.RefObject<TextInput>
  thirdInputRef: React.RefObject<TextInput>
  fourthInputRef: React.RefObject<TextInput>
  otpIndexes: TOtpIndex
  onChangeTextHandler: (params: OnChangeHandlerParaTypes) => void
  shouldOTPFormBeRed: boolean
}

function FormSection(props: Props) {
  const {
    firstInputRef,
    secondInputRef,
    thirdInputRef,
    fourthInputRef,
    otpIndexes,
    shouldOTPFormBeRed,
    onChangeTextHandler,
  } = props

  const refArrays = Array<React.RefObject<TextInput>>(NUMBER_OF_OTP_DIGITS)

  refArrays[0] = firstInputRef
  refArrays[1] = secondInputRef
  refArrays[2] = thirdInputRef
  refArrays[3] = fourthInputRef

  return (
    <View style={ styles.container }>
      { refArrays.map((ref_, index) => (
        <View style={ shouldOTPFormBeRed ? [styles.box, styles.otpBoxRed] : styles.box } key={ index }>
          <TextInput
            ref={ refArrays[index] }
            style={ styles.inputField }
            keyboardType="number-pad"
            maxLength={ 1 }
            // @ts-ignore
            value={ otpIndexes[index + 1] }
            onChangeText={ (text: string) =>
              // @ts-ignore
              onChangeTextHandler({ text, inputIndex: index + 1 })
            }
          />
        </View>
      )) }
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    flexDirection: "row",
    marginTop: "10%",
  },
  box: {
    width: 60,
    height: 55,
    backgroundColor: "rgba(255, 255, 255, 0.1)",
    marginRight: 12,
    borderRadius: 10,
    alignItems: "center",
    borderStyle: "solid",
    borderColor: "red",
  },
  otpBoxRed: {
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: colors.APP_RED_COLOR
  },
  inputField: {
    width: "100%",
    height: "100%",
    color: "white",
    textAlign: "center",
    fontFamily: "Urbanist",
    fontSize: 20
  },
})

export default FormSection
