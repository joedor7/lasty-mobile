import React from "react"
import { StyleSheet, View } from "react-native"

// Local imports
import { OTP_FLOW_TEXT } from "../../../utils/text/OTP_flow_text"
import { Typography } from "../../../UIcomponents"
import { useAppSelector } from "../../../redux/hooks"

function TextSection() {
  const email = useAppSelector(state => state.user.email)

  return (
    <View style={ styles.container }>
      <Typography sx={ styles.sectionTitle }>
        { OTP_FLOW_TEXT.ENTER_OTP_PAGE_TITLE }
      </Typography>
      <Typography sx={ styles.sectionSub }>
        { OTP_FLOW_TEXT.ENTER_OTP_PAGE_SUB }
        <Typography sx={ styles.userEmail }>{ email }</Typography>
      </Typography>
    </View>
  )
}

const styles = StyleSheet.create({
  container: { width: "100%" },
  sectionTitle: { fontWeight: "bold", fontSize: 30, color: "white" },
  sectionSub: { color: "white", fontSize: 16, marginTop: "2%" },
  userEmail: {
    color: "white",
    fontWeight: "bold",
    fontSize: 16,
    textDecorationStyle: "solid",
    textDecorationColor: "white",
    textDecorationLine: "underline",
  },
})

export default TextSection
