import React from "react"
import { Pressable, StyleSheet, View } from "react-native"
import { Feather } from "@expo/vector-icons"

// local imports
import { colors } from "../../../theme/colors"
import IllustrationSection from "./IllustrationSection"
import TextSection from "./TextSection"
import FormSection from "./FormSection"
import useFormSection from "./hooks/userFormSection"
import useOTPVerification from "./hooks/useOTPverification"
import { useAppSelector } from "../../../redux/hooks"
import OTPVerificationSpinner from "./OTPVerificationSpinner"

function TopSection() {
  const user = useAppSelector(state => state.user)

  const {
    firstInputRef,
    secondInputRef,
    thirdInputRef,
    fourthInputRef,
    otpIndexes,
    onChangeTextHandler,
  } = useFormSection()

  const { handleVerifyOTP, isLoading, transformOTPToString, shouldOTPFormBeRed } = useOTPVerification()

  return (
    <View style={ styles.container }>
      <View style={ styles.base }>
        <IllustrationSection />
        <TextSection />
        <FormSection
          shouldOTPFormBeRed={ shouldOTPFormBeRed }
          firstInputRef={ firstInputRef }
          secondInputRef={ secondInputRef }
          thirdInputRef={ thirdInputRef }
          fourthInputRef={ fourthInputRef }
          otpIndexes={ otpIndexes }
          onChangeTextHandler={ onChangeTextHandler }
        />


        {/* Button that will show the next step on creating an account */ }
        <Pressable
          disabled={ transformOTPToString(otpIndexes).length < 4 }
          style={ styles.nextButton }
          //@ts-ignore
          onPress={ () => handleVerifyOTP({ email: user.email, otp: transformOTPToString(otpIndexes) }) }
        >
          {
            isLoading ? <OTPVerificationSpinner /> :
              <Feather name="check" size={ 24 } color={ colors.WHITE_BG } />
          }
        </Pressable>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "45%",
    // backgroundColor: colors.PRIMARY,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
  },
  base: {
    flex: 1,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    paddingHorizontal: "4%",
    alignItems: "center",
    justifyContent: "flex-end",
    paddingBottom: "10%",
  },
  nextButton: {
    flexDirection: "row",
    position: "absolute",
    bottom: -25,
    justifyContent: "center",
    paddingHorizontal: "3%",
    height: 50,
    width: 50,
    alignItems: "center",
    backgroundColor: colors.PRIMARY,
    borderRadius: 50,
    borderColor: "rgba(255, 255, 255, 0.4)",
    borderStyle: "solid",
    borderWidth: 1,
  },
  btnTextStyle: {
    color: "white",
  },
})

export default TopSection
