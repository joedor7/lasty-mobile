import React from "react";
import { StyleSheet, View } from "react-native";

// local imports
import { colors } from "../../../theme/colors";
import TopSection from "./TopSection";

function OTPVerificationScreen() {
  return (
    <View style={styles.container}>
      <TopSection />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.GRAY_BG,
  },

  topSection: {
    width: "100%",
    height: "50%",
    // backgroundColor: colors.PRIMARY,
    justifyContent: "flex-end",
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
  },
});

export default OTPVerificationScreen;
