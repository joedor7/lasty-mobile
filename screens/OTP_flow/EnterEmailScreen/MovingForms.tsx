import { Dimensions, StyleSheet, View } from 'react-native'
import React from 'react'
import LottieView from 'lottie-react-native'

// Getting the window height
const windowHeight = Dimensions.get("window").height
const windowWidth = Dimensions.get("window").height

export default function MovingFormsAnimation() {
  return (
    <View style={ styles.container }>
      <View style={ styles.animationWrapper }>
        <View style={ styles.animationContainer }>
          <LottieView
            resizeMode='cover'
            source={ require("../../../assets/lotties/moving_forms.json") }
            style={ styles.animation }
            autoPlay
          />
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: "95%",
    minHeight: windowHeight,
    position: "absolute",
    justifyContent: "flex-end",
    alignItems: "flex-end",
    flex: 1,
    zIndex: -1
  },
  animationWrapper: {
    right: "-5%",
    width: windowWidth,
  },
  animationContainer: {
    height: 500,
    right: "-60%",

  },
  animation: {
    // bottom: 0,
    width: "100%",
    height: "100%",
    padding: 0,
    margin: 0
  }
})