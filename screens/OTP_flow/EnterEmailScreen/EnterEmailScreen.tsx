import React from "react"
import { StyleSheet, View } from "react-native"
import { NativeStackScreenProps } from "@react-navigation/native-stack"

// Local imports
import { colors } from "../../../theme/colors"
import TopSection from "./TopSection"
import { MainNativeStackParamList } from "../../../types/NavigationTypes/MainNativeStackTypes"
import MovingFormsAnimation from "./MovingForms"

type Props = NativeStackScreenProps<MainNativeStackParamList>


function EnterEmailScreen({ navigation, route }: Props) {

  return (
    <View style={ styles.base }>
      <TopSection />
      {/* <MovingFormsAnimation /> */ }
    </View>
  )
}

export const styles = StyleSheet.create({
  base: {
    flex: 1,
    backgroundColor: colors.GRAY_BG,
  },
})

export default EnterEmailScreen
