import { useState } from 'react'
import { useNavigation } from '@react-navigation/native'
import { NativeStackScreenProps } from '@react-navigation/native-stack'

// Local imports
import { MainNativeStackParamList } from '../../../../types/NavigationTypes/MainNativeStackTypes'
import { useGetOTPMutation } from '../../../../redux/apiSlices/userSlices'
import isValidEmail from '../../../../utils/isValidEmail'
import { useAppDispatch } from '../../../../redux/hooks'
import { setUserCredentials } from '../../../../redux/features/user/userSlice'

export function useGetOTPWithMail() {
  const [emailInput, setEmailInput] = useState("")
  const dispatch = useAppDispatch()

  const [getOTP, { isLoading }] = useGetOTPMutation()

  const navigation =
    useNavigation<NativeStackScreenProps<MainNativeStackParamList>>()

  // Verify if the user enters a valid email
  const isEmailValid = (isValidEmail(emailInput))

  // This function will get triggered when the caller trying to get a OTP
  async function handleGetOTP() {

    try {
      await getOTP({ email: emailInput })

      dispatch(setUserCredentials({ modifiedField: "email", email: emailInput }))

      // @ts-ignore
      navigation.navigate("OTP Verification")

    } catch (error) {
      console.log(error)
    }
  }

  return {
    emailInput,
    setEmailInput,
    handleGetOTP,
    isLoading,
    isEmailValid
  }
}