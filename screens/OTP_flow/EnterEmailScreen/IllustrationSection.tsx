import React from "react";
import { Image, StyleSheet, View } from "react-native";

function IllustrationSection() {
  return (
    <View style={styles.base}>
      <Image
        style={styles.imgStyles}
        source={require("../../../assets/images/104.png")}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  base: {
    width: "100%",
  },
  imgStyles: {
    width: 100,
    height: 100,
  },
});

export default IllustrationSection;
