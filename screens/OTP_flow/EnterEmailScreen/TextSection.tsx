import React from "react";
import { View, StyleSheet } from "react-native";

// @local imports
import { Typography } from "../../../UIcomponents";
import { registerScreenText } from "../../../utils/text/Register/register.text";

export default function TextSection() {
  return (
    <View style={style.base}>
      <Typography sx={style.welcomeTextStyle}>
        {registerScreenText.GET_STARTED_TEXT}
      </Typography>
      <Typography sx={style.descriptionText}>
        {registerScreenText.SECTION_DES}
      </Typography>
    </View>
  );
}

const style = StyleSheet.create({
  base: {
    width: "100%",
  },
  welcomeTextStyle: {
    fontWeight: "bold",
    fontSize: 30,
    color: "white",
  },
  descriptionText: {
    color: "white",
    fontSize: 16,
    marginTop: "2%",
  },
});
