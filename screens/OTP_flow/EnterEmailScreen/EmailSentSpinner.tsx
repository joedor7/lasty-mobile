import React from "react"
import LottieView from "lottie-react-native"
import { StyleSheet, View } from "react-native"

export default function EmailSentSpinner() {
  return (
    <View>
      <LottieView
        source={ require("../../../assets/lotties/email_sent_animation.json") }
        style={ styles.animation }
        autoPlay
      />
    </View>
  )
}

const styles = StyleSheet.create({
  animation: {
    width: 100,
    height: 100,
  },
})