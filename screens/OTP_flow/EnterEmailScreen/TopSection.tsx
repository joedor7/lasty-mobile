import React from "react"
import { View, StyleSheet, Pressable } from "react-native"
import { Feather } from "@expo/vector-icons"

// Local imports
import IllustrationSection from "./IllustrationSection"
import TextSection from "./TextSection"
import FormSection from "./FormSection"
import { colors } from "../../../theme/colors"
import { useGetOTPWithMail } from "./hooks/useGetOTPWithMail"
import EmailSentSpinner from "./EmailSentSpinner"

export default function TopSection() {
  const { emailInput, setEmailInput, handleGetOTP, isEmailValid, isLoading } = useGetOTPWithMail()

  return (
    <View style={ style.container }>
      <View style={ style.base }>
        <IllustrationSection />
        <TextSection />
        <FormSection emailInput={ emailInput } setEmailInput={ setEmailInput } />

        {/* Button that will show the next step on creating an account */ }
        <Pressable
          style={ style.nextButton }
          onPress={ handleGetOTP }
          disabled={ (!isEmailValid || isLoading) }
        >
          {
            isLoading ? EmailSentSpinner :
              <Feather name="arrow-right" size={ 24 } color={ colors.WHITE_BG } />
          }
        </Pressable>
      </View>
    </View>
  )
}

const style = StyleSheet.create({
  container: {
    width: "100%",
    height: "45%",
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
  },
  base: {
    flex: 1,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    paddingHorizontal: "4%",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  nextButton: {
    flexDirection: "row",
    position: "absolute",
    bottom: -25,
    justifyContent: "center",
    paddingHorizontal: "3%",
    height: 50,
    width: 50,
    alignItems: "center",
    backgroundColor: colors.PRIMARY,
    borderRadius: 50,
    borderColor: "rgba(255, 255, 255, 0.4)",
    borderStyle: "solid",
    borderWidth: 1,
  },
  btnTextStyle: {
    color: "white",
  },
})
