import React from "react"
import { View, StyleSheet, TextInput } from "react-native"

// @local imports
import { registerFormSectionText } from "../../../utils/text/Register/formSection.text"

interface Props {
  emailInput: string
  setEmailInput: React.Dispatch<React.SetStateAction<string>>
}

export default function FormSection(props: Props) {

  const { emailInput, setEmailInput } = props

  return (
    <View style={ style.container }>
      <TextInput
        value={ emailInput }
        onChangeText={ (text) => setEmailInput(text) }
        placeholder={ registerFormSectionText.EMAIL_PLACEHOLDER }
        style={ style.textInput }
        textContentType="emailAddress"
        autoComplete="email"
        inputMode="email"
        keyboardType="email-address"
        maxLength={ 60 }
        cursorColor={ "white" }
        placeholderTextColor={ "rgba(255, 255, 255, 1)" }
      />
    </View>
  )
}

const style = StyleSheet.create({
  container: {
    marginVertical: "10%",
    width: "100%",
    alignItems: "center",
  },
  textInput: {
    width: "100%",
    borderRadius: 10,
    paddingHorizontal: "2%",
    color: "white",
    paddingVertical: "4%",
    backgroundColor: "rgba(255, 255, 255, 0.1)",
  },
  passwordInput: {
    marginTop: "4%",
  },
})
