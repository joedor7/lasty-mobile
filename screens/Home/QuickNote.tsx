import React from "react";
import { View, StyleSheet, Dimensions, Image } from "react-native";

// local imports
import { Typography } from "../../UIcomponents";
import { colors } from "../../theme/colors";

// Getting the dimension of the window because we are going to use it later on
const windowWidth = Dimensions.get("window").width;

interface IQuickNote {
  idea: {
    note_idea: string;
    people_who_wrote: string[];
  };
}

export default function QuickNote(props: IQuickNote) {
  const { idea } = props;

  return (
    <View style={styles.container}>
      <View style={styles.noteIdea}>
        <Typography sx={styles.noteIdeaText}>{idea.note_idea}</Typography>
      </View>
      <View style={styles.notePeopleSection}>
        <View style={styles.pfpsContainer}>
          {idea.people_who_wrote.map((pfp, index) => (
            <Image
              source={pfp}
              style={{ ...styles.pfp, left: index * 10 }}
              key={index}
            />
          ))}
        </View>
        <Typography sx={styles.peopleCount}>
          500 people wrote about this
        </Typography>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: windowWidth * 0.75,
    marginRight: 25,
  },
  noteIdea: {
    width: "100%",
    height: "55%",
    backgroundColor: colors.GRAY_ACCENT,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
    paddingHorizontal: 10,
    marginBottom: 10,
  },
  noteIdeaText: {
    fontSize: 16,
  },
  notePeopleSection: {
    flexDirection: "row",
    justifyContent: "flex-end",
    height: 25,
    marginLeft: 10,
  },
  pfpsContainer: {
    position: "relative",
    flexDirection: "row",
    left: 10,
  },
  pfp: {
    position: "absolute",
    width: 25,
    height: 25,
  },
  peopleCount: {
    marginLeft: 70,
    fontSize: 16,
  },
});
