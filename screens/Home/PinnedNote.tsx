import React from "react";
import {
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Image,
} from "react-native";

// local imports
import { colors } from "../../theme/colors";
import { Typography } from "../../UIcomponents";
import NoteBottomSection from "./NoteBottomSection";

const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;

interface Props {
  note: {
    title: string;
    content: string;
    writer: any[];
    last_modified: string;
    color: string;
    tag: string;
  };
}

export default function PinnedNote(props: Props) {
  return (
    <TouchableOpacity style={styles.container}>
      <View style={styles.colorAndCategory}>
        <View
          style={{ ...styles.noteColor, backgroundColor: props.note.color }}
        />
        <Typography sx={styles.categoryText}>{props.note.tag}</Typography>
      </View>
      <Typography style={styles.noteTitle} numberOfLines={1}>
        {props.note.title}
      </Typography>

      <NoteBottomSection
        last_modified={props.note.last_modified}
        writer={props.note.writer}
      />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: windowWidth * 0.8,
    height: windowHeight * 0.1,
    borderRadius: 15,
    backgroundColor: colors.GRAY_ACCENT,
    marginRight: 20,
    paddingVertical: 8,
    paddingHorizontal: 20,
    justifyContent: "center",
  },
  colorAndCategory: {
    justifyContent: "center",
  },
  noteColor: {
    width: 10,
    height: 10,
    left: -14,
    borderRadius: 50,
    position: "absolute",
  },
  categoryText: {
    fontSize: 16,
  },
  noteTitle: {
    fontSize: 20,
    color: "white",
  },
  noteContent: {
    fontSize: 15,
  },
  bottomSection: {
    marginTop: "3%",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  writersPfpSection: {
    flexDirection: "row",
  },
  writersTxt: {
    fontSize: 16,
  },
  pfpsContainer: {
    position: "relative",
    left: 10,
  },
  pfp: {
    position: "absolute",
    width: 20,
    height: 20,
  },
  lastModifiedText: {
    fontSize: 16,
  },
});
