import React, { useCallback, useMemo, useRef } from "react"
import { StyleSheet, SafeAreaView, ScrollView, View, Text } from "react-native"

// local imports
import { colors } from "../../theme/colors"
import Header from "./Header"
import PinnedSection from "./PinnedNoteSection"
import RecentNotesSection from "./RecentNotesSection"
import NoteIdeasSection from "./NoteIdeasSection"
import BottomSheet from "@gorhom/bottom-sheet"
import ProfileComponent from "./ProfileComponent"
import EmptyNotesSection from "./EmptyNotesSection"

export default function Home() {
  const bottomSheetRef = useRef<BottomSheet>(null)

  // variables
  const snapPoints = useMemo(() => ["70%", "85%"], [])

  // callbacks
  const handleSheetChanges = useCallback((index: number) => {
    console.log("handleSheetChanges", index)
  }, [])

  return (
    <SafeAreaView style={ styles.container }>
      <Header bottomSheetRef={ bottomSheetRef } />
      <ScrollView
        showsVerticalScrollIndicator={ false }
        style={ styles.scrollContainer }
        contentContainerStyle={ styles.scrollContentStyle }
      >
        <NoteIdeasSection />
        <PinnedSection />
        {/* <RecentNotesSection /> */ }
        { <EmptyNotesSection /> }
      </ScrollView>
      <BottomSheet
        enablePanDownToClose
        ref={ bottomSheetRef }
        index={ -1 }
        snapPoints={ snapPoints }
        onChange={ handleSheetChanges }
        backgroundStyle={ { backgroundColor: "#1E1E1E" } }
      >
        <ProfileComponent />
      </BottomSheet>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.GRAY_BG,
    alignItems: "center",
  },
  scrollContainer: {
    width: "100%",
  },
  scrollContentStyle: {
    alignItems: "center",
  },
})
