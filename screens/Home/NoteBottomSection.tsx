import React from "react";
import { View, StyleSheet, Image } from "react-native";

// local imports
import { Typography } from "../../UIcomponents";

interface Props {
  writer: any[];
  last_modified: string;
}

export default function NoteBottomSection(props: Props) {
  return (
    <View style={styles.container}>
      <View style={styles.writersPfpSection}>
        <Typography sx={styles.writersTxt}>Writers:</Typography>
        <View style={styles.pfpsContainer}>
          {props.writer.map((writer_pfp, index) => (
            <Image source={writer_pfp} style={styles.pfp} key={index} />
          ))}
        </View>
      </View>
      <Typography sx={styles.lastModifiedText}>
        {props.last_modified}
      </Typography>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: "3%",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  writersPfpSection: {
    flexDirection: "row",
  },
  writersTxt: {
    fontSize: 16,
  },
  pfpsContainer: {
    position: "relative",
    left: 10,
  },
  pfp: {
    position: "absolute",
    width: 20,
    height: 20,
  },
  lastModifiedText: {
    fontSize: 16,
  },
});
