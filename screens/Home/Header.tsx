import React from "react"
import { View, StyleSheet, TouchableOpacity, Image } from "react-native"
import { Feather } from "@expo/vector-icons"
import { NativeStackScreenProps } from "@react-navigation/native-stack"
import { useNavigation } from "@react-navigation/native"

// Local imports
import { colors } from "../../theme/colors"
import { userMock } from "../../mocks/data"
import { Typography } from "../../UIcomponents"
import { MainNativeStackParamList } from "../../types/NavigationTypes/MainNativeStackTypes"
import { BottomSheetMethods } from "@gorhom/bottom-sheet/lib/typescript/types"
import { useAppSelector } from "../../redux/hooks"

interface HeaderProps {
  bottomSheetRef: React.RefObject<BottomSheetMethods>
}

export default function Header(props: HeaderProps) {
  const { bottomSheetRef } = props

  const user = useAppSelector(state => state.user)

  const navigation =
    useNavigation<NativeStackScreenProps<MainNativeStackParamList>>()

  return (
    <View style={ styles.container }>
      <TouchableOpacity
        style={ styles.pfpContainer }
        onPress={ () => bottomSheetRef.current?.expand() }
      >
        <Image source={ userMock.pfp } style={ styles.pfp } />
      </TouchableOpacity>
      <Typography sx={ { marginLeft: 10 } }>@{ user.username ?? "Laster" }</Typography>

      {/* New note button */ }
      <TouchableOpacity
        style={ styles.newNoteBtn }
        // @ts-ignore
        onPress={ () => navigation.navigate("Create Note") }
      >
        {/* <Typography sx={styles.newNoteBtnText}>New</Typography> */ }
        <Feather name="file-plus" color={ "white" } size={ 18 } />
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: "95%",
    height: "10%",
    borderRadius: 10,
    marginTop: "1%",
    flexDirection: "row",
    alignItems: "center",
  },
  pfp: {
    width: 35,
    height: 35,
  },
  pfpContainer: {
    width: 50,
    height: 50,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: "rgba(255, 255, 255, 0.34)",
    borderStyle: "solid",
    alignItems: "center",
    justifyContent: "center",
  },

  newNoteBtn: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 13,
    height: 37,
    backgroundColor: colors.SECONDARY_ORANGE,
    borderRadius: 15,
    position: "absolute",
    right: 10,
  },
  newNoteBtnText: {
    marginRight: 5,
    color: "white",
    fontSize: 17,
  },
})
