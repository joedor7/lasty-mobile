import React from "react"
import { View, StyleSheet } from "react-native"

// local imports
import { Typography } from "../../UIcomponents"
import RecentNote from "./RecentNote"
import { recentNotesMock } from "../../mocks/data"

export default function RecentNotesSection() {
  return (
    <View style={ styles.container }>
      <Typography sx={ styles.sectionTitle }>Recent Notes</Typography>

      <View style={ styles.recentNotesContainer }>
        { recentNotesMock.map((item, index) => (
          <RecentNote note={ item } key={ index } />
        )) }
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: "95%",
    // marginTop: 50,
  },
  sectionTitle: {
    fontSize: 20,
  },
  recentNotesContainer: {
    width: "100%",
    marginTop: 20,
  },
})
