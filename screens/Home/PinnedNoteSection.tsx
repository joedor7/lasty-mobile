import React from "react"
import { View, StyleSheet, FlatList } from "react-native"

// local imports
import { Typography } from "../../UIcomponents"
import PinnedNote from "./PinnedNote"
import { pinnedNotesMock } from "../../mocks/data"

export default function PinnedSection() {
  return (
    <View style={ styles.container }>
      <Typography sx={ styles.sectionTitle }>Pinned Notes</Typography>
      <View style={ styles.pinnedNotesContainer }>
        <FlatList
          data={ pinnedNotesMock }
          renderItem={ (item) => <PinnedNote note={ item.item } /> }
          horizontal
          showsHorizontalScrollIndicator={ false }
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: "95%",
    marginTop: 20,
  },
  sectionTitle: {
    fontSize: 20,
  },
  pinnedNotesContainer: {
    width: "100%",
    // flex: 1,
    marginTop: 20,
  },
})
