import React from 'react'
import { View, StyleSheet, Pressable, Dimensions } from 'react-native'
import LottieView from 'lottie-react-native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'
import { useNavigation } from '@react-navigation/native'

// Local imports
import { Typography } from '../../UIcomponents'
import { colors } from '../../theme/colors'
import { MainNativeStackParamList } from '../../types/NavigationTypes/MainNativeStackTypes'

const windowHeight = Dimensions.get("window").height

export default function EmptyNotesSection() {
  const navigation =
    useNavigation<NativeStackNavigationProp<MainNativeStackParamList>>()

  return (
    <View style={ styles.container }>
      <View style={ styles.animationWrapper }>
        <View style={ styles.animationContainer }>
          <LottieView
            source={ require("../../assets/lotties/Thinking_To_Note.json") }
            style={ styles.animation }
            autoPlay
            loop
          />
        </View>
      </View>
      <Typography style={ styles.startFromScratchText }>
        Start writing now
      </Typography>
      <Typography sx={ styles.subtitle }>
        For now you don't have any notes. Create a new note by pressing Create a new note
      </Typography>

      <Pressable style={ styles.createNoteButton } onPress={ () => navigation.navigate("Create Note") }>
        <Typography>Create a new note</Typography>
      </Pressable>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: "95%",
    minHeight: windowHeight * .2,
    borderRadius: 10,
    paddingLeft: "4%",
    justifyContent: "center",
    borderWidth: 2,
    borderStyle: "solid",
    borderColor: colors.GRAY_ACCENT,
    paddingBottom: "6%"
  },
  startFromScratchText: {
    fontWeight: "bold",
    color: "white",
    fontSize: 27,
  },
  subtitle: {
    fontSize: 18,
    marginTop: "2.4%",
    color: "white",
    marginBottom: "3.4%"
  },
  animationWrapper: {
  },
  animationContainer: {
    position: "relative",
    width: 150,
    height: 150,
    borderRadius: 10
  },
  animation: {
    position: "absolute",
    left: -20,
    width: 150,
    height: 150,
    padding: 0,
    margin: 0

  },
  createNoteButton: {
    marginTop: "3.3%",
    backgroundColor: colors.PRIMARY,
    width: "60%",
    paddingVertical: 11,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10
  }
})