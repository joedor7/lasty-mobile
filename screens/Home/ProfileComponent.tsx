import React from "react"
import { View, StyleSheet, Image, TouchableOpacity } from "react-native"
import { Feather } from "@expo/vector-icons"

// local imports
import { Typography } from "../../UIcomponents"
import { colors } from "../../theme/colors"
import { userMock } from "../../mocks/data"

export default function ProfileComponent() {
  return (
    <View style={ styles.container }>
      <View style={ styles.profileInfo }>
        <View style={ styles.pfpContainer }>
          <Image source={ userMock.pfp } style={ styles.pfp } />
        </View>
        <View style={ styles.userInfoSection }>
          <Typography numberOfLines={ 1 } style={ styles.userNameSx }>
            Jeff Dorsainvil
          </Typography>
          <Typography numberOfLines={ 1 } style={ styles.userNotesCount }>
            10 notes
          </Typography>
          <Typography numberOfLines={ 1 } style={ styles.userEmail }>
            joedor7@gmail.com
          </Typography>
        </View>
      </View>

      {/* TODO: Remove duplicated code and refactor this into its own component */ }
      <View style={ styles.bottomButtons }>
        <TouchableOpacity style={ styles.bottomButton }>
          <Feather
            name="log-out"
            color="white"
            size={ 20 }
            style={ { position: "absolute", left: 10 } }
          />
          <Typography>Logout</Typography>
        </TouchableOpacity>
        <TouchableOpacity style={ styles.bottomButton }>
          <Feather
            name="share"
            color="white"
            size={ 20 }
            style={ { position: "absolute", left: 10 } }
          />
          <Typography>Share Lasty</Typography>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: "5%",
    paddingVertical: "5%",
    justifyContent: "space-between",
  },
  profileInfo: {
    flexDirection: "row",
    backgroundColor: colors.GRAY_BG,
    borderRadius: 15,
    paddingVertical: "4%",
    paddingHorizontal: "4%",
  },
  pfpContainer: {
    width: 40,
    height: 40,
    borderRadius: 50,
    marginRight: "4%",
  },
  pfp: { width: "100%", height: "100%" },
  userInfoSection: {},
  userNameSx: {
    fontWeight: "bold",
    color: "white",
    fontSize: 20,
    marginBottom: "3%",
  },
  userNotesCount: {
    color: "white",
    marginBottom: "3%",
  },
  userEmail: {
    color: colors.PRIMARY,
  },

  bottomButtons: {
    width: "100%",
  },
  bottomButton: {
    position: "relative",
    justifyContent: "center",
    width: "100%",
    borderRadius: 15,
    backgroundColor: colors.GRAY_BG,
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: "5%",
    marginBottom: "5%",
  },
})
