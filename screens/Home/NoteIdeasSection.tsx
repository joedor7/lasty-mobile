import React from "react"
import { View, StyleSheet, FlatList, Dimensions } from "react-native"

// local imports
import { Typography } from "../../UIcomponents"
import { quickNoteMock } from "../../mocks/data"
import { HomeText } from "../../utils/text/HomeText/home"
import QuickNote from "./QuickNote"
import { useAppSelector } from "../../redux/hooks"

const windowHeight = Dimensions.get("window").height

export default function NoteIdeasSection() {
  const user = useAppSelector(state => state.user)

  return (
    <View style={ styles.container }>
      <View style={ styles.Textsection }>
        <Typography style={ styles.helloText }>
          { `${HomeText.HELLO_TEXT}${user.username ?? "Laster"}` }
        </Typography>
        <Typography sx={ styles.subtitle }>
          { HomeText.QUICK_NOTE_SECTION_TITLE }
        </Typography>
      </View>

      <View style={ styles.quickNotesSection }>
        <FlatList
          data={ quickNoteMock }
          renderItem={ (item) => <QuickNote idea={ item.item } key={ item.index } /> }
          horizontal
          showsHorizontalScrollIndicator={ false }
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: "95%",
    height: windowHeight * 0.25,
  },
  Textsection: {},
  helloText: {
    fontWeight: "bold",
    color: "white",
    fontSize: 27,
  },
  subtitle: {
    fontSize: 18,
    marginTop: "2.4%",
  },
  quickNotesSection: {
    marginTop: "7%",
  },
})
