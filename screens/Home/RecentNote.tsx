import React from "react";
import { View, StyleSheet, Dimensions } from "react-native";

//  local imports
import { colors } from "../../theme/colors";
import { Typography } from "../../UIcomponents";
import NoteBottomSection from "./NoteBottomSection";

const windowHeight = Dimensions.get("window").height;

interface Props {
  note: {
    title: string;
    content: string;
    writer: any[];
    last_modified: string;
    color: string;
    tag: string;
  };
}

export default function RecentNote(props: Props) {
  return (
    <View style={styles.container}>
      <View style={styles.colorAndCategory}>
        <View
          style={{ ...styles.noteColor, backgroundColor: props.note.color }}
        />
        <Typography sx={styles.categoryText}>{props.note.tag}</Typography>
      </View>

      <Typography style={styles.noteTitle} numberOfLines={1}>
        {props.note.title}
      </Typography>

      <Typography sx={styles.noteContent} numberOfLines={3}>
        {props.note.content}
      </Typography>

      <NoteBottomSection
        last_modified={props.note.last_modified}
        writer={props.note.writer}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    marginBottom: 15,
    height: windowHeight * 0.18,
    justifyContent: "space-between",
    backgroundColor: colors.GRAY_ACCENT,
    borderRadius: 15,
    paddingVertical: 8,
    paddingHorizontal: 20,
  },
  colorAndCategory: {
    justifyContent: "center",
  },
  noteColor: {
    width: 10,
    height: 10,
    left: -14,
    borderRadius: 50,
    position: "absolute",
  },
  categoryText: {
    fontSize: 16,
  },
  noteTitle: {
    fontSize: 21,
    color: "white",
  },
  noteContent: {
    fontSize: 15,
  },
});
