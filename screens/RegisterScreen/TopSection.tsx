import React from "react";
import { View, StyleSheet } from "react-native";

// local imports
import { Typography } from "../../UIcomponents";

export default function TopSection() {
  return (
    <View style={styles.container}>
      <Typography sx={styles.title}>Create your account</Typography>
      <Typography sx={styles.sub}>
        Please choose a username and a profile picture to finish creating your
        account
      </Typography>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "80%",
    alignItems: "center",
    marginTop: "20%",
  },
  title: {
    fontWeight: "bold",
    fontSize: 30,
    color: "white",
    textAlign: "center",
    marginBottom: "3%",
  },
  sub: {
    fontSize: 16,
    color: "white",
    textAlign: "center",
  },
});
