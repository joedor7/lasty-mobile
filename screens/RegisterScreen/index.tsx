import React from "react";
import { StyleSheet } from "react-native";

// local imports
import { colors } from "../../theme/colors";
import TopSection from "./TopSection";
import { SafeAreaView } from "react-native-safe-area-context";
import ChooseUsernameAndPfp from "./ChooseUsernameAndPfp";

function RegisterScreen() {
  return (
    <SafeAreaView style={styles.base}>
      <TopSection />
      <ChooseUsernameAndPfp />
    </SafeAreaView>
  );
}

export const styles = StyleSheet.create({
  base: {
    flex: 1,
    backgroundColor: colors.GRAY_BG,
    paddingTop: "4%",
    alignItems: "center",
  },
});

export default RegisterScreen;
