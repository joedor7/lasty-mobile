import React from "react"
import { View, StyleSheet, TextInput, TouchableOpacity } from "react-native"

// local imports
import { Typography } from "../../UIcomponents"
import { colors } from "../../theme/colors"
import useCreateUserForm from "./hooks/useCreateUserForm"

export default function ChooseUsernameAndPfp() {

  const { username, setUsername, handleCreateAccount } = useCreateUserForm()

  return (
    <View style={ styles.container }>
      <View style={ styles.usernameTextInputSection }>
        <Typography style={ styles.usernameTextInputLabel }>
          Choose a username
        </Typography>
        <TextInput style={ styles.usernameTextInput }
          value={ username }
          onChangeText={ text => setUsername(text) }
        />
      </View>

      <TouchableOpacity
        style={ styles.createAccountButton }
        onPress={ handleCreateAccount }
      >
        <Typography style={ styles.createAccountBtnLabel }>
          Create account
        </Typography>
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "90%",
    marginTop: "15%",
    alignItems: "center",
  },
  pfpContainer: {
    width: 90,
    height: 90,
    borderRadius: 50,
    backgroundColor: "white",
  },
  editPictureText: {
    color: colors.PRIMARY,
    fontSize: 17,
    marginTop: "4%",
  },
  usernameTextInputSection: {
    width: "90%",
    marginTop: "10%",
  },
  usernameTextInputLabel: {
    color: "white",
    marginBottom: "3%",
  },
  usernameTextInput: {
    paddingLeft: "3%",
    color: "white",
    fontFamily: "Urbanist",
    width: "100%",
    height: 55,
    borderRadius: 10,
    backgroundColor: "rgba(255, 255, 255, 0.1)",
  },
  createAccountButton: {
    width: "90%",
    backgroundColor: colors.PRIMARY,
    alignItems: "center",
    justifyContent: "center",
    height: 55,
    borderRadius: 10,
    position: "absolute",
    bottom: "5%",
  },
  createAccountBtnLabel: {
    color: "white",
    fontSize: 17,
  },
})
