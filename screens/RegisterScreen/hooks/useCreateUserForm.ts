import { useState } from "react"
import { useNavigation } from "@react-navigation/native"
import { NativeStackNavigationProp } from "@react-navigation/native-stack"

// Local imports
import { useAppSelector } from "../../../redux/hooks"
import { useCreateUserMutation } from "../../../redux/apiSlices/userSlices"
import { MainNativeStackParamList } from "../../../types/NavigationTypes/MainNativeStackTypes"

export default function useCreateUserForm() {
  const [username, setUsername] = useState("")
  const userStore = useAppSelector(state => state.user)

  const [createUser, { isLoading }] = useCreateUserMutation()

  const navigation =
    useNavigation<NativeStackNavigationProp<MainNativeStackParamList>>()


  async function handleCreateAccount() {
    try {

      if (username && userStore.email && userStore.otp) {
        const res = await createUser({ username, email: userStore.email, otp: userStore.otp }).unwrap()

        console.log(res.data)

        if (res.data) navigation.navigate("Home")

      }
    } catch (error) {
      console.log(error)
    }
  }

  return { username, setUsername, isLoading, handleCreateAccount }
}