import React from "react";
import { View, StyleSheet, TextInput, TouchableOpacity } from "react-native";
import { MaterialCommunityIcons, Feather } from "@expo/vector-icons";

// local imports
import ColorAndNoteActionSection from "./ColorAndNoteActionSection";
import ActionButton from "./ActionButton";
import { colors } from "../../theme/colors";
import { useNavigation } from "@react-navigation/native";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { MainNativeStackParamList } from "types/NavigationTypes/MainNativeStackTypes";

export default function NoteViewHeader() {
  const navigation =
    useNavigation<NativeStackScreenProps<MainNativeStackParamList>>();

  return (
    <View style={styles.container}>
      <View style={styles.backAndTitleSection}>
        <View style={styles.backAndTitleInputContainer}>
          <TouchableOpacity onPress={() => navigation.navigate("Home")}>
            <Feather name="chevron-left" color={"white"} size={35} />
          </TouchableOpacity>
          <TextInput
            placeholder="Title"
            style={styles.titleInputStyle}
            placeholderTextColor={"#fff"}
          />
        </View>
        <View style={styles.pinAndMoreSection}>
          <ActionButton
            children={
              <MaterialCommunityIcons
                name="pin-outline"
                size={20}
                color="white"
              />
            }
            backgroundColor={colors.GRAY_ACCENT}
          />
        </View>
      </View>
      <ColorAndNoteActionSection />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "95%",
    height: "15%",
    alignItems: "flex-start",
    justifyContent: "center",
  },
  backAndTitleSection: {
    flexDirection: "row",
    marginBottom: "5%",
    left: -10,
  },
  backAndTitleInputContainer: {
    flexDirection: "row",
    width: "90%",
  },
  titleInputStyle: {
    fontFamily: "Urbanist",
    fontWeight: "bold",
    fontSize: 20,
    color: "white",
    width: "87%",
  },
  pinAndMoreSection: {},
});
