import React, { useState } from "react";
import { View, StyleSheet } from "react-native";

// local imports
import { ALLOWED_NOTE_COLORS, colors } from "../../theme/colors";
import NoteColor from "./NoteColor";
import ActionButton from "./ActionButton";
import { Feather } from "@expo/vector-icons";

export default function ColorAndNoteActionSection() {
  const [activeColor, setActiveColor] = useState<string>(
    colors.SECONDARY_ORANGE
  );

  return (
    <View style={styles.container}>
      <View style={styles.noteColors}>
        {ALLOWED_NOTE_COLORS.map((note) => (
          <NoteColor
            setActiveColor={setActiveColor}
            key={note}
            backgroundColor={note}
            isActive={activeColor === note}
          />
        ))}
      </View>
      <View style={styles.actionButtons}>
        <ActionButton
          backgroundColor={colors.PRIMARY}
          children={<Feather name="check" color={"#FFF"} size={15} />}
        />

        <ActionButton
          backgroundColor={"#FF5858"}
          children={<Feather name="trash-2" color={"#FFF"} size={15} />}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "95%",
    flexDirection: "row",
    justifyContent: "space-between",
  },

  noteColors: {
    width: "70%",
    flexDirection: "row",
  },

  actionButtons: {
    flexDirection: "row",
    width: "20%",
    justifyContent: "space-between",
  },
});
