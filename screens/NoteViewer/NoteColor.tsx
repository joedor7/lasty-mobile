import React from "react";
import {
  View,
  StyleSheet,
  StyleProp,
  ViewStyle,
  TouchableOpacity,
} from "react-native";

// Local imports
import { colors } from "../../theme/colors";

interface NoteColorProps {
  backgroundColor: string;
  isActive: boolean;
  setActiveColor: React.Dispatch<React.SetStateAction<string>>;
}

export default function NoteColor(props: NoteColorProps) {
  const { backgroundColor, isActive, setActiveColor } = props;

  const noteColorStyle: StyleProp<ViewStyle> = {
    ...styles.noteColorStyle,
    backgroundColor,
  };

  const noteContainerStyle = isActive
    ? { ...styles.colorContainer, ...styles.activeColorBorder }
    : { ...styles.colorContainer };

  return (
    <TouchableOpacity
      style={noteContainerStyle}
      onPress={(_) => setActiveColor(backgroundColor)}
    >
      <View style={noteColorStyle} />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  colorContainer: {
    width: 30,
    height: 30,
    marginRight: "4%",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 50,
  },

  activeColorBorder: {
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: colors.WHITE_BG,
  },

  noteColorStyle: {
    width: 17,
    height: 17,
    borderRadius: 50,
  },
});
