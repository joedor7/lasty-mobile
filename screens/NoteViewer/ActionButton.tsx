import React from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";

interface ActionButtonProps {
  children: JSX.Element;
  backgroundColor: string;
}

export default function ActionButton(props: ActionButtonProps) {
  const { backgroundColor, children } = props;
  return (
    <TouchableOpacity style={{ ...styles.container, backgroundColor }}>
      {children}
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: 30,
    height: 30,
    borderRadius: 50,
    backgroundColor: "blue",
    alignItems: "center",
    justifyContent: "center",
  },
});
