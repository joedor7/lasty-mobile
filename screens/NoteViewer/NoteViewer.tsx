import React from "react";
import { StyleSheet, SafeAreaView } from "react-native";

// local imports
import { colors } from "../../theme/colors";
import NoteViewHeader from "./NoteViewHeader";
import NoteTakingSection from "./NoteTakingSection";
import ColorAndNoteActionSection from "./ColorAndNoteActionSection";

export default function NoteViewer() {
  return (
    <SafeAreaView style={styles.container}>
      <NoteViewHeader />
      <NoteTakingSection />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.GRAY_BG,
    alignItems: "center",
  },
});
