import React from "react";
import { View, StyleSheet, Dimensions, TextInput } from "react-native";

// local imports
import { colors } from "../../theme/colors";

const windowHeight = Dimensions.get("window").height;

export default function NoteTakingSection() {
  return (
    <View style={styles.container}>
      <TextInput style={styles.noteTakingInput} multiline />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: windowHeight * 0.8,
    backgroundColor: colors.GRAY_ACCENT,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    position: "absolute",
    bottom: 0,
    alignItems: "center",
    paddingTop: "6%",
  },
  noteTakingInput: {
    width: "95%",
    height: "100%",
    // borderColor: "red",
    // borderWidth: 1,
    // borderStyle: "solid",
    color: "white",
    fontFamily: "Urbanist",
    fontSize: 17,
  },
});
