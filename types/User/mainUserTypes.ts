interface IUser {
  otp: string
  username: string
  email: string
  pfp_url: string
  notes: string[]
  interests: string[]
  level: number
  pk: string
  followers: [string]
  following: [string]
  savedNotes: [string]
  pinnedNotes: [string]
}

interface ICreateUserForm {
  username: string
  email: string
  otp: string
}

export type { IUser, ICreateUserForm }