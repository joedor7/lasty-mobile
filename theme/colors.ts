export const colors = {
  PRIMARY: "#4E3CF1",
  SECONDARY_ORANGE: "#EC753A",
  GRAY_BG: "#121213",
  GRAY_ACCENT: "rgba(225, 228, 229, 0.09)",
  WHITE_BG: "#FFF",
  APP_RED_COLOR: "#D14E4E"
}

export const ALLOWED_NOTE_COLORS = [
  colors.PRIMARY,
  colors.SECONDARY_ORANGE,
  "#3381FF",
  "#EF5DA8",
  "#F72585",
]
