import React from "react";
import { StyleSheet, Text, TextProps, TextStyle } from "react-native";

interface Props extends TextProps {
  children: React.ReactNode;
  sx?: TextStyle;
  weight?: TextStyle["fontWeight"];
}

export default function Typography(props: Props) {
  const { children, sx, weight, ...rest } = props;

  const finalStyle: TextProps = {
    ...typographyStyles.baseStyle,
    fontWeight: weight,
    ...sx,
  };

  return (
    <Text style={finalStyle} {...rest}>
      {children}
    </Text>
  );
}

const typographyStyles = StyleSheet.create({
  baseStyle: {
    color: "white",
    fontFamily: "Urbanist",
    fontSize: 20,
  },
});
