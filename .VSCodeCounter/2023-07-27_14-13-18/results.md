# Summary

Date : 2023-07-27 14:13:18

Directory /home/nova/Documents/lasty-mobile

Total : 37 files,  21823 codes, 30 comments, 132 blanks, all 21985 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 3 | 20,931 | 0 | 3 | 20,934 |
| TypeScript JSX | 23 | 787 | 30 | 111 | 928 |
| TypeScript | 8 | 77 | 0 | 14 | 91 |
| JSON with Comments | 1 | 22 | 0 | 2 | 24 |
| JavaScript | 1 | 6 | 0 | 1 | 7 |
| Markdown | 1 | 0 | 0 | 1 | 1 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 37 | 21,823 | 30 | 132 | 21,985 |
| . (Files) | 7 | 20,975 | 1 | 11 | 20,987 |
| UIcomponents | 2 | 29 | 0 | 8 | 37 |
| UIcomponents (Files) | 1 | 2 | 0 | 2 | 4 |
| UIcomponents/Typography | 1 | 27 | 0 | 6 | 33 |
| navigation | 1 | 43 | 2 | 6 | 51 |
| navigation/MainNativeStack | 1 | 43 | 2 | 6 | 51 |
| screens | 20 | 743 | 27 | 100 | 870 |
| screens/Home | 6 | 183 | 9 | 27 | 219 |
| screens/OTP_flow | 11 | 425 | 14 | 58 | 497 |
| screens/OTP_flow/EnterEmailScreen | 5 | 176 | 7 | 24 | 207 |
| screens/OTP_flow/OTPVerificationScreen | 6 | 249 | 7 | 34 | 290 |
| screens/OTP_flow/OTPVerificationScreen (Files) | 5 | 207 | 7 | 28 | 242 |
| screens/OTP_flow/OTPVerificationScreen/hooks | 1 | 42 | 0 | 6 | 48 |
| screens/RegisterScreen | 3 | 135 | 4 | 15 | 154 |
| theme | 1 | 7 | 0 | 1 | 8 |
| types | 1 | 6 | 0 | 1 | 7 |
| types/NavigationTypes | 1 | 6 | 0 | 1 | 7 |
| utils | 5 | 20 | 0 | 5 | 25 |
| utils (Files) | 2 | 5 | 0 | 2 | 7 |
| utils/text | 3 | 15 | 0 | 3 | 18 |
| utils/text (Files) | 1 | 5 | 0 | 1 | 6 |
| utils/text/Register | 2 | 10 | 0 | 2 | 12 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)