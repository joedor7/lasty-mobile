# Details

Date : 2023-07-27 14:19:51

Directory /home/nova/Documents/lasty-mobile

Total : 36 files,  936 codes, 30 comments, 131 blanks, all 1097 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [App.tsx](/App.tsx) | TypeScript JSX | 16 | 1 | 4 | 21 |
| [README.md](/README.md) | Markdown | 0 | 0 | 1 | 1 |
| [UIcomponents/Typography/Typography.tsx](/UIcomponents/Typography/Typography.tsx) | TypeScript JSX | 27 | 0 | 6 | 33 |
| [UIcomponents/index.ts](/UIcomponents/index.ts) | TypeScript | 2 | 0 | 2 | 4 |
| [app.json](/app.json) | JSON | 12 | 0 | 1 | 13 |
| [babel.config.js](/babel.config.js) | JavaScript | 6 | 0 | 1 | 7 |
| [navigation/MainNativeStack/MainNativeStack.tsx](/navigation/MainNativeStack/MainNativeStack.tsx) | TypeScript JSX | 43 | 2 | 6 | 51 |
| [package.json](/package.json) | JSON | 32 | 0 | 1 | 33 |
| [screens/Home/Header.tsx](/screens/Home/Header.tsx) | TypeScript JSX | 71 | 3 | 5 | 79 |
| [screens/Home/Home.tsx](/screens/Home/Home.tsx) | TypeScript JSX | 22 | 1 | 4 | 27 |
| [screens/Home/PinnedNote.tsx](/screens/Home/PinnedNote.tsx) | TypeScript JSX | 16 | 1 | 5 | 22 |
| [screens/Home/PinnedNoteSection.tsx](/screens/Home/PinnedNoteSection.tsx) | TypeScript JSX | 31 | 2 | 4 | 37 |
| [screens/Home/RecentNote.tsx](/screens/Home/RecentNote.tsx) | TypeScript JSX | 16 | 1 | 5 | 22 |
| [screens/Home/RecentNotesSection.tsx](/screens/Home/RecentNotesSection.tsx) | TypeScript JSX | 27 | 1 | 4 | 32 |
| [screens/OTP_flow/EnterEmailScreen/EnterEmailScreen.tsx](/screens/OTP_flow/EnterEmailScreen/EnterEmailScreen.tsx) | TypeScript JSX | 21 | 2 | 6 | 29 |
| [screens/OTP_flow/EnterEmailScreen/FormSection.tsx](/screens/OTP_flow/EnterEmailScreen/FormSection.tsx) | TypeScript JSX | 38 | 1 | 4 | 43 |
| [screens/OTP_flow/EnterEmailScreen/IllustrationSection.tsx](/screens/OTP_flow/EnterEmailScreen/IllustrationSection.tsx) | TypeScript JSX | 22 | 0 | 4 | 26 |
| [screens/OTP_flow/EnterEmailScreen/TextSection.tsx](/screens/OTP_flow/EnterEmailScreen/TextSection.tsx) | TypeScript JSX | 31 | 1 | 4 | 36 |
| [screens/OTP_flow/EnterEmailScreen/TopSection.tsx](/screens/OTP_flow/EnterEmailScreen/TopSection.tsx) | TypeScript JSX | 64 | 3 | 6 | 73 |
| [screens/OTP_flow/OTPVerificationScreen/FormSection.tsx](/screens/OTP_flow/OTPVerificationScreen/FormSection.tsx) | TypeScript JSX | 61 | 2 | 7 | 70 |
| [screens/OTP_flow/OTPVerificationScreen/IllustrationSection.tsx](/screens/OTP_flow/OTPVerificationScreen/IllustrationSection.tsx) | TypeScript JSX | 22 | 0 | 4 | 26 |
| [screens/OTP_flow/OTPVerificationScreen/OTPVerificationScreen.tsx](/screens/OTP_flow/OTPVerificationScreen/OTPVerificationScreen.tsx) | TypeScript JSX | 26 | 1 | 6 | 33 |
| [screens/OTP_flow/OTPVerificationScreen/TextSection.tsx](/screens/OTP_flow/OTPVerificationScreen/TextSection.tsx) | TypeScript JSX | 31 | 1 | 5 | 37 |
| [screens/OTP_flow/OTPVerificationScreen/TopSection.tsx](/screens/OTP_flow/OTPVerificationScreen/TopSection.tsx) | TypeScript JSX | 67 | 3 | 6 | 76 |
| [screens/OTP_flow/OTPVerificationScreen/hooks/userFormSection.ts](/screens/OTP_flow/OTPVerificationScreen/hooks/userFormSection.ts) | TypeScript | 42 | 0 | 6 | 48 |
| [screens/RegisterScreen/ChooseUsernameAndPfp.tsx](/screens/RegisterScreen/ChooseUsernameAndPfp.tsx) | TypeScript JSX | 79 | 2 | 6 | 87 |
| [screens/RegisterScreen/TopSection.tsx](/screens/RegisterScreen/TopSection.tsx) | TypeScript JSX | 33 | 1 | 4 | 38 |
| [screens/RegisterScreen/index.tsx](/screens/RegisterScreen/index.tsx) | TypeScript JSX | 23 | 1 | 5 | 29 |
| [theme/colors.ts](/theme/colors.ts) | TypeScript | 7 | 0 | 1 | 8 |
| [tsconfig.json](/tsconfig.json) | JSON with Comments | 22 | 0 | 2 | 24 |
| [types/NavigationTypes/MainNativeStackTypes.ts](/types/NavigationTypes/MainNativeStackTypes.ts) | TypeScript | 6 | 0 | 1 | 7 |
| [utils/loadFonts.tsx](/utils/loadFonts.tsx) | TypeScript JSX | 0 | 0 | 1 | 1 |
| [utils/navigationNames.ts](/utils/navigationNames.ts) | TypeScript | 5 | 0 | 1 | 6 |
| [utils/text/OTP_flow_text.ts](/utils/text/OTP_flow_text.ts) | TypeScript | 5 | 0 | 1 | 6 |
| [utils/text/Register/formSection.text.ts](/utils/text/Register/formSection.text.ts) | TypeScript | 4 | 0 | 1 | 5 |
| [utils/text/Register/register.text.ts](/utils/text/Register/register.text.ts) | TypeScript | 6 | 0 | 1 | 7 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)