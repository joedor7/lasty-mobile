Date : 2023-07-27 14:19:51
Directory : /home/nova/Documents/lasty-mobile
Total : 36 files,  936 codes, 30 comments, 131 blanks, all 1097 lines

Languages
+--------------------+------------+------------+------------+------------+------------+
| language           | files      | code       | comment    | blank      | total      |
+--------------------+------------+------------+------------+------------+------------+
| TypeScript JSX     |         23 |        787 |         30 |        111 |        928 |
| TypeScript         |          8 |         77 |          0 |         14 |         91 |
| JSON               |          2 |         44 |          0 |          2 |         46 |
| JSON with Comments |          1 |         22 |          0 |          2 |         24 |
| JavaScript         |          1 |          6 |          0 |          1 |          7 |
| Markdown           |          1 |          0 |          0 |          1 |          1 |
+--------------------+------------+------------+------------+------------+------------+

Directories
+----------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| path                                                                                               | files      | code       | comment    | blank      | total      |
+----------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| .                                                                                                  |         36 |        936 |         30 |        131 |      1,097 |
| . (Files)                                                                                          |          6 |         88 |          1 |         10 |         99 |
| UIcomponents                                                                                       |          2 |         29 |          0 |          8 |         37 |
| UIcomponents (Files)                                                                               |          1 |          2 |          0 |          2 |          4 |
| UIcomponents/Typography                                                                            |          1 |         27 |          0 |          6 |         33 |
| navigation                                                                                         |          1 |         43 |          2 |          6 |         51 |
| navigation/MainNativeStack                                                                         |          1 |         43 |          2 |          6 |         51 |
| screens                                                                                            |         20 |        743 |         27 |        100 |        870 |
| screens/Home                                                                                       |          6 |        183 |          9 |         27 |        219 |
| screens/OTP_flow                                                                                   |         11 |        425 |         14 |         58 |        497 |
| screens/OTP_flow/EnterEmailScreen                                                                  |          5 |        176 |          7 |         24 |        207 |
| screens/OTP_flow/OTPVerificationScreen                                                             |          6 |        249 |          7 |         34 |        290 |
| screens/OTP_flow/OTPVerificationScreen (Files)                                                     |          5 |        207 |          7 |         28 |        242 |
| screens/OTP_flow/OTPVerificationScreen/hooks                                                       |          1 |         42 |          0 |          6 |         48 |
| screens/RegisterScreen                                                                             |          3 |        135 |          4 |         15 |        154 |
| theme                                                                                              |          1 |          7 |          0 |          1 |          8 |
| types                                                                                              |          1 |          6 |          0 |          1 |          7 |
| types/NavigationTypes                                                                              |          1 |          6 |          0 |          1 |          7 |
| utils                                                                                              |          5 |         20 |          0 |          5 |         25 |
| utils (Files)                                                                                      |          2 |          5 |          0 |          2 |          7 |
| utils/text                                                                                         |          3 |         15 |          0 |          3 |         18 |
| utils/text (Files)                                                                                 |          1 |          5 |          0 |          1 |          6 |
| utils/text/Register                                                                                |          2 |         10 |          0 |          2 |         12 |
+----------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+

Files
+----------------------------------------------------------------------------------------------------+--------------------+------------+------------+------------+------------+
| filename                                                                                           | language           | code       | comment    | blank      | total      |
+----------------------------------------------------------------------------------------------------+--------------------+------------+------------+------------+------------+
| /home/nova/Documents/lasty-mobile/App.tsx                                                          | TypeScript JSX     |         16 |          1 |          4 |         21 |
| /home/nova/Documents/lasty-mobile/README.md                                                        | Markdown           |          0 |          0 |          1 |          1 |
| /home/nova/Documents/lasty-mobile/UIcomponents/Typography/Typography.tsx                           | TypeScript JSX     |         27 |          0 |          6 |         33 |
| /home/nova/Documents/lasty-mobile/UIcomponents/index.ts                                            | TypeScript         |          2 |          0 |          2 |          4 |
| /home/nova/Documents/lasty-mobile/app.json                                                         | JSON               |         12 |          0 |          1 |         13 |
| /home/nova/Documents/lasty-mobile/babel.config.js                                                  | JavaScript         |          6 |          0 |          1 |          7 |
| /home/nova/Documents/lasty-mobile/navigation/MainNativeStack/MainNativeStack.tsx                   | TypeScript JSX     |         43 |          2 |          6 |         51 |
| /home/nova/Documents/lasty-mobile/package.json                                                     | JSON               |         32 |          0 |          1 |         33 |
| /home/nova/Documents/lasty-mobile/screens/Home/Header.tsx                                          | TypeScript JSX     |         71 |          3 |          5 |         79 |
| /home/nova/Documents/lasty-mobile/screens/Home/Home.tsx                                            | TypeScript JSX     |         22 |          1 |          4 |         27 |
| /home/nova/Documents/lasty-mobile/screens/Home/PinnedNote.tsx                                      | TypeScript JSX     |         16 |          1 |          5 |         22 |
| /home/nova/Documents/lasty-mobile/screens/Home/PinnedNoteSection.tsx                               | TypeScript JSX     |         31 |          2 |          4 |         37 |
| /home/nova/Documents/lasty-mobile/screens/Home/RecentNote.tsx                                      | TypeScript JSX     |         16 |          1 |          5 |         22 |
| /home/nova/Documents/lasty-mobile/screens/Home/RecentNotesSection.tsx                              | TypeScript JSX     |         27 |          1 |          4 |         32 |
| /home/nova/Documents/lasty-mobile/screens/OTP_flow/EnterEmailScreen/EnterEmailScreen.tsx           | TypeScript JSX     |         21 |          2 |          6 |         29 |
| /home/nova/Documents/lasty-mobile/screens/OTP_flow/EnterEmailScreen/FormSection.tsx                | TypeScript JSX     |         38 |          1 |          4 |         43 |
| /home/nova/Documents/lasty-mobile/screens/OTP_flow/EnterEmailScreen/IllustrationSection.tsx        | TypeScript JSX     |         22 |          0 |          4 |         26 |
| /home/nova/Documents/lasty-mobile/screens/OTP_flow/EnterEmailScreen/TextSection.tsx                | TypeScript JSX     |         31 |          1 |          4 |         36 |
| /home/nova/Documents/lasty-mobile/screens/OTP_flow/EnterEmailScreen/TopSection.tsx                 | TypeScript JSX     |         64 |          3 |          6 |         73 |
| /home/nova/Documents/lasty-mobile/screens/OTP_flow/OTPVerificationScreen/FormSection.tsx           | TypeScript JSX     |         61 |          2 |          7 |         70 |
| /home/nova/Documents/lasty-mobile/screens/OTP_flow/OTPVerificationScreen/IllustrationSection.tsx   | TypeScript JSX     |         22 |          0 |          4 |         26 |
| /home/nova/Documents/lasty-mobile/screens/OTP_flow/OTPVerificationScreen/OTPVerificationScreen.tsx | TypeScript JSX     |         26 |          1 |          6 |         33 |
| /home/nova/Documents/lasty-mobile/screens/OTP_flow/OTPVerificationScreen/TextSection.tsx           | TypeScript JSX     |         31 |          1 |          5 |         37 |
| /home/nova/Documents/lasty-mobile/screens/OTP_flow/OTPVerificationScreen/TopSection.tsx            | TypeScript JSX     |         67 |          3 |          6 |         76 |
| /home/nova/Documents/lasty-mobile/screens/OTP_flow/OTPVerificationScreen/hooks/userFormSection.ts  | TypeScript         |         42 |          0 |          6 |         48 |
| /home/nova/Documents/lasty-mobile/screens/RegisterScreen/ChooseUsernameAndPfp.tsx                  | TypeScript JSX     |         79 |          2 |          6 |         87 |
| /home/nova/Documents/lasty-mobile/screens/RegisterScreen/TopSection.tsx                            | TypeScript JSX     |         33 |          1 |          4 |         38 |
| /home/nova/Documents/lasty-mobile/screens/RegisterScreen/index.tsx                                 | TypeScript JSX     |         23 |          1 |          5 |         29 |
| /home/nova/Documents/lasty-mobile/theme/colors.ts                                                  | TypeScript         |          7 |          0 |          1 |          8 |
| /home/nova/Documents/lasty-mobile/tsconfig.json                                                    | JSON with Comments |         22 |          0 |          2 |         24 |
| /home/nova/Documents/lasty-mobile/types/NavigationTypes/MainNativeStackTypes.ts                    | TypeScript         |          6 |          0 |          1 |          7 |
| /home/nova/Documents/lasty-mobile/utils/loadFonts.tsx                                              | TypeScript JSX     |          0 |          0 |          1 |          1 |
| /home/nova/Documents/lasty-mobile/utils/navigationNames.ts                                         | TypeScript         |          5 |          0 |          1 |          6 |
| /home/nova/Documents/lasty-mobile/utils/text/OTP_flow_text.ts                                      | TypeScript         |          5 |          0 |          1 |          6 |
| /home/nova/Documents/lasty-mobile/utils/text/Register/formSection.text.ts                          | TypeScript         |          4 |          0 |          1 |          5 |
| /home/nova/Documents/lasty-mobile/utils/text/Register/register.text.ts                             | TypeScript         |          6 |          0 |          1 |          7 |
| Total                                                                                              |                    |        936 |         30 |        131 |      1,097 |
+----------------------------------------------------------------------------------------------------+--------------------+------------+------------+------------+------------+